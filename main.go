package main

import (
	"bufio"
	_ "embed"
	"fmt"
	"os"
	"strings"
)

//go:embed dict.txt
var rawDict string

type key string

// Strings with their runes sorted alphabetically will generate the same keys
// if they are anagrams
func genKey(s string) key {
	return key(sortString(strings.ToLower(s)))
}

func main() {
	// Argument handling
	if len(os.Args) != 2 {
		fmt.Println(
			"Provide exactly 1 argument: the word to search for anagrams of",
		)
		os.Exit(1)
	}
	searchFor := os.Args[1]

	// Build dictionary
	dict := make(map[key][]string)

	fileScanner := bufio.NewScanner(strings.NewReader(rawDict))
	fileScanner.Split(bufio.ScanLines)
	for fileScanner.Scan() {
		v := fileScanner.Text()
		k := genKey(v)
		dict[k] = append(dict[k], v)
	}

	// Now generate a key for the word we want anagrams of
	k := genKey(searchFor)
	v, found := dict[k]
	if !found {
		fmt.Println("no matches")
		os.Exit(0)
	}
	fmt.Println(v)
}
